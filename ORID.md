# O

- Understand training rules.

- Participated in ice break.

- I gained an understanding of How to learn - (PDCA) and discussed with my group to come up with a learning plan.

- Learned concept maps and worked with the group to try building a concept map about the internet.

- Understand the significance of Standup Meeting.

# R

This is a fruitful day.

# I

Further understanding of classmates through ice break; By studying PDCA, I have gained a more specific understanding of future reading and learning plans; By learning concept maps, a new approach and method has been provided for solving complex problems in the future; After understanding the significance of Standup Meeting, we should have a serious attitude towards it in the future.

# D

I will try to learn what I learned today in my future studies and work.